package cat.sparking;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

public class PopUpMessage extends DialogFragment {
	public static final int METERS = 0;
	public static final int TIME = 1;
	private Button mButton;
	private TextView mTextMessage;
	
	private String mMessage;

	@SuppressLint("ValidFragment")
	public PopUpMessage(String message) {
		mMessage = message;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		getDialog().setCancelable(false);
		View v = inflater.inflate(R.layout.popup_message, container);
		
		mButton = (Button) v.findViewById(R.id.popup_message_button);
		mTextMessage = (TextView) v.findViewById(R.id.popUp_message_Text);
		
		mButton.setTypeface(SparkingApplication.typeRobotoMedium);
		mTextMessage.setTypeface(SparkingApplication.typeRobotoMedium);
		
		mTextMessage.setText(mMessage);
		
		mButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
					dismiss();
				}
		});
		return v;
	}
}