package cat.sparking;

import android.app.DialogFragment;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class LoadingProgressDialog extends DialogFragment {
	
	private String message = "Cargando...";
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setCancelable(false);
		
		setStyle(DialogFragment.STYLE_NO_INPUT, R.style.DialogStyle);
		View v = inflater.inflate(R.layout.dialog_loading, container);
		
		ImageView img = (ImageView) v.findViewById(R.id.infinite_loop);
		final AnimationDrawable anim = (AnimationDrawable) img.getDrawable();
		TextView text = (TextView) v.findViewById(R.id.dialog_text);
		text.setText(message);
		
		img.post(new Runnable() {
			@Override
			public void run() {
				anim.start();
			}
		});
		return v;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}
