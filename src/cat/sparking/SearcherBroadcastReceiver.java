package cat.sparking;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class SearcherBroadcastReceiver extends WakefulBroadcastReceiver{

	public static final String TAG = SearcherBroadcastReceiver.class.getCanonicalName();
	public static String ACTION_ALARM = "cat.sparking.alarmmanager.alarm";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();
		String action = bundle.getString(ACTION_ALARM);
		
		if (action.equals(ACTION_ALARM)) {
			ComponentName comp = new ComponentName(context.getPackageName(), SearcherTaskService.class.getName());
			intent.setComponent(comp);
			startWakefulService(context, intent);
			setResultCode(Activity.RESULT_OK);
		}
	}

}
