package cat.sparking;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import cat.sparking.restclientbbva.SearcherRestClient;
import cat.sparking.restclientbbva.models.Searcher;

public class SearcherTaskService extends IntentService {
	private final String TAG = SearcherTaskService.class.getCanonicalName();

	public final static String RESULT_UPDATE_SEARCHER = "result_update_searcher_succeeded";
	public final static String EXTRAS_UPDATE_SEARCHER = "extras_update_searcher";

	public String mDataReturn = null;
	private SparkingApplication mApplication;

	public SearcherTaskService() {
		super("SearcherTaskService");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mApplication = (SparkingApplication) getApplication();
		if (mApplication == null) {
			Log.i(TAG, "APPLICATION ESTA PER TANCAR");
		}
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		if (mApplication == null) {
			Log.i(TAG, "APPLICATION ESTA PER TANCAR2");
		}
		if (mApplication.getSearcher() == null) {
			Log.i(TAG, "EL QUE ESTA PER TANCAR ES EL SEARCHER!");
		}
		Searcher searcher = mApplication.getSearcher();
		searcher.putLatitude(mApplication.getLatitude());
		searcher.putLongitude(mApplication.getLongitude());
		SearcherRestClient rc = new SearcherRestClient();

		JSONObject o = null;
		try {
			o = rc.update(String.valueOf(searcher.getId()), searcher);
		} catch (JSONException e) {
			Log.e(TAG, "Registration error: ", e);
		} catch (NullPointerException e2) {
			Log.e(TAG, "Problem with internet connection");
		}
		sendSearchers(o.toString());
		SearcherBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendSearchers(String searchers) {
		Intent intent = new Intent(RESULT_UPDATE_SEARCHER);
		intent.putExtra(EXTRAS_UPDATE_SEARCHER, searchers);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}
}
