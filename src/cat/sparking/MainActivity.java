package cat.sparking;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
//import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cat.sparking.SparkingService.SparkingServiceBinder;
import cat.sparking.restclientbbva.models.Leaver;
import cat.sparking.restclientbbva.models.Searcher;
import cat.sparking.restclientbbva.models.SearcherResponse;
import cat.sparking.utils.ConnectionState;
import cat.sparking.utils.VerifyJSONObjects;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity implements SparkingService.SparkingServiceListener,
		PopUpSelectTime.OnTimeSelectedListener {

	public static final String TAG = MainActivity.class.getCanonicalName();
	public final int REQUEST_CODE = 1234;

	private GoogleMap mMap;
	private SparkingApplication mApplication;
	private ConnectionState mNetState;
	private SparkingService mService;
	private boolean mBound;
	private static final float ZOOM_DEFAULT = 17;
	private static final int MAX_POINTS = 10;
	LoadingProgressDialog mLoading;
	private CountDownTimer mCountDownTimer = null;

	private LocationManager mLocationManager;

	private Button mButtonSearch;
	private Button mButtonReserve;
	private Button mButtonLeaving;
	private boolean mPressendSearch = false;
	private ImageView mButtonProfile;
	private TextView mImageClock;

	private boolean syncStatus = false;
	private boolean internetStatus = false;

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private final static String SENDER_ID = "653506150399";
	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private GoogleCloudMessaging mGCM;
	private Context mContext;
	private Criteria mCriteria;

	private ArrayList<Leaver> mLeaversList = null;
	private ArrayList<Searcher> mSearchersList = null;
	private int mTimeElapsed = 0;
	private int mTimeSelected = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);

		getActionBar().hide();
		mApplication = (SparkingApplication) getApplication();
		mApplication.loadPreferences();

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(SearcherTaskService.RESULT_UPDATE_SEARCHER);
		intentFilter.addAction(GcmIntentService.RESULT_UPDATE_LEAVER);
		LocalBroadcastManager.getInstance(this).registerReceiver(mSearchersReceiver, intentFilter);

		mButtonSearch = (Button) findViewById(R.id.button_search);
		mButtonReserve = (Button) findViewById(R.id.button_reserve);
		mButtonLeaving = (Button) findViewById(R.id.button_leave);
		mButtonProfile = (ImageView) findViewById(R.id.button_user);
		mImageClock = (TextView) findViewById(R.id.img_logo_clicable);

		mButtonSearch.setTypeface(SparkingApplication.typeRobotoBold);
		mButtonReserve.setTypeface(SparkingApplication.typeRobotoBold);
		mButtonLeaving.setTypeface(SparkingApplication.typeRobotoBold);

		mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fixed_map)).getMap();

		mNetState = new ConnectionState(this);

		mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		mCriteria = new Criteria();
		mCriteria.setAccuracy(Criteria.ACCURACY_FINE);
		mCriteria.setAltitudeRequired(false);
		mCriteria.setBearingRequired(false);
		mCriteria.setCostAllowed(true);
		mCriteria.setPowerRequirement(Criteria.POWER_LOW);

		String provider = mLocationManager.getBestProvider(mCriteria, true);
		Location location = mLocationManager.getLastKnownLocation(provider);
		updateWithNewLocation(location);

		mLocationManager.requestLocationUpdates(provider, 1000, 0, locationListener);

		mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

		if (!mNetState.isConnectingToInternet()) {
			Toast.makeText(this, "No interent connection", Toast.LENGTH_LONG).show();
			internetStatus = false;
		} else {
			internetStatus = true;
		}

		if (checkPlayServices()) {
			mMap.clear();
			mMap.setMyLocationEnabled(true);
			mMap.getUiSettings().setAllGesturesEnabled(true);
			mMap.getUiSettings().setZoomControlsEnabled(true);
			mMap.animateCamera(CameraUpdateFactory.zoomIn());
			mMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_DEFAULT), 2000, null);

			mGCM = GoogleCloudMessaging.getInstance(this);
			if (mApplication.getGcmId() == null) {
				registerInBackground();
			}
		}

		mButtonSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!syncStatus) {
					if (mApplication.getState() == SparkingApplication.STATE_IDLE) {
						// PopUpSelectTime fragment = new
						// PopUpSelectTime(PopUpSelectTime.METERS,
						// "Rango de búsqueda");
						// fragment.show(getFragmentManager(), "");
						getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
						syncStatus = true;
						mLoading = new LoadingProgressDialog();
						mLoading.show(getFragmentManager(), "");
						if (!mService.newSearcher(MainActivity.this, makeSearcher(1000))) {
							// Log.e(TAG, "Wrong call.");
						}
					} else if (mApplication.getState() == SparkingApplication.STATE_SEARCHING) {
						getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
						if (mService == null) {
							// Log.i(TAG, "Service error.");
						} else {
							syncStatus = true;
							mLoading = new LoadingProgressDialog();
							mLoading.show(getFragmentManager(), "");
							mPressendSearch = true;
							if (!mService.removeSearcher(MainActivity.this, mApplication.getSearcher())) {
								// Log.e(TAG, "Wrong call.");
							}
						}
					}
				}
			}
		});

		mButtonReserve.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mApplication.getState().equals(SparkingApplication.STATE_IDLE)) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							mMap.clear();
							mApplication.setSearcher(null);
							mApplication.setState(SparkingApplication.STATE_PARKED);
							mApplication.setLeaver(makeLeaver());
							mMap.addMarker(setLeaver(mApplication.getLeaver()));
							setLayout();
						}
					});
				} else if (mApplication.getState() == SparkingApplication.STATE_SEARCHING) {
					getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
					if (mService == null) {
						// Log.i(TAG, "Service error.");
					} else {
						mLoading = new LoadingProgressDialog();
						mLoading.show(getFragmentManager(), "");
						mPressendSearch = false;
						if (!mService.removeSearcher(MainActivity.this, mApplication.getSearcher())) {
							// Log.e(TAG, "Wrong call.");
						}
					}
				}
			}
		});

		mButtonLeaving.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mApplication.getState() == SparkingApplication.STATE_PARKED) {
					PopUpSelectTime fragment = new PopUpSelectTime(PopUpSelectTime.TIME, "Me voy en...");
					fragment.show(getFragmentManager(), "123");
				}
			}
		});

		mButtonProfile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
				startActivity(intent);
			}
		});

		mImageClock.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mCountDownTimer.onFinish();
				mCountDownTimer.cancel();
				mCountDownTimer = null;
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		mApplication.loadPreferences();
		if (!mNetState.isConnectingToInternet()) {
			Toast.makeText(this, "No interent connection", Toast.LENGTH_LONG).show();
			internetStatus = false;
		} else {
			internetStatus = true;
		}
		setLayout();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mApplication.getState() == SparkingApplication.STATE_SEARCHING) {
			getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			mPressendSearch = true;
			if (!mService.removeSearcher(MainActivity.this, mApplication.getSearcher())) {
				// Log.e(TAG, "Wrong call.");
			}
			mApplication.setState(SparkingApplication.STATE_IDLE);
		}
		mApplication.savePreferences();
	}

	@Override
	public void onStart() {
		super.onStart();
		Intent intent = new Intent(this, SparkingService.class);
		bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	public void onStop() {
		super.onStop();
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
	}

	@Override
	public void onDestroy() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(mSearchersReceiver);
		super.onDestroy();
	}

	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			SparkingServiceBinder binder = (SparkingServiceBinder) service;
			mService = binder.getService();
			mBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			mBound = false;
		}
	};

	private final LocationListener locationListener = new LocationListener() {

		@Override
		public void onLocationChanged(Location location) {
			updateWithNewLocation(location);
		}

		@Override
		public void onProviderDisabled(String provider) {
			updateWithNewLocation(null);
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

	};

	private void updateWithNewLocation(Location location) {
		if (location != null) {
			mApplication.setLocation(location.getLatitude(), location.getLongitude());
		}
	}

	private void registerInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (mGCM == null) {
						mGCM = GoogleCloudMessaging.getInstance(mContext);
					}
					mApplication.setGcmId(mGCM.register(SENDER_ID));
				} catch (IOException ex) {
					// Log.e(TAG, ex.getMessage());
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String result) {

			}
		}.execute(null, null, null);
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				finish();
			}
			return false;
		}
		return true;
	}

	public void startSchedule() {
		try {
			AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
			Intent intent = new Intent(getApplicationContext(), SearcherBroadcastReceiver.class);
			intent.putExtra(SearcherBroadcastReceiver.ACTION_ALARM, SearcherBroadcastReceiver.ACTION_ALARM);

			final PendingIntent pIntent = PendingIntent.getBroadcast(this, REQUEST_CODE, intent,
					PendingIntent.FLAG_UPDATE_CURRENT);
			alarm.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 15 * 1000, pIntent);
			Toast.makeText(getApplicationContext(), "Busqueda iniciada...", Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			// Log.e(TAG, "Error startSchedule()" + e.getMessage());
		}
	}

	public void cancelSchedule() {
		try {
			Intent intent = new Intent(getApplicationContext(), SearcherBroadcastReceiver.class);
			intent.putExtra(SearcherBroadcastReceiver.ACTION_ALARM, SearcherBroadcastReceiver.ACTION_ALARM);

			final PendingIntent pIntent = PendingIntent.getBroadcast(this, REQUEST_CODE, intent,
					PendingIntent.FLAG_UPDATE_CURRENT);
			AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
			alarm.cancel(pIntent);
			Toast.makeText(getApplicationContext(), "Busqueda finalizada...", Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			// Log.e(TAG, "Error cancelSchedule()" + e.getMessage());
		}
	}

	public Searcher makeSearcher(int meters) {
		Searcher s = new Searcher();
		s.putGcmId(mApplication.getGcmId());
		s.putLatitude(mApplication.getLatitude());
		s.putLongitude(mApplication.getLongitude());
		s.putRadius((float) meters);
		return s;
	}

	public Leaver makeLeaver() {
		Leaver p = new Leaver();
		p.putGcmId(mApplication.getGcmId());
		p.putLatitude(mApplication.getLatitude());
		p.putLongitude(mApplication.getLongitude());
		return p;
	}

	public void setLayout() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mMap.clear();
				if (!internetStatus) {
					setDisabled(mButtonSearch);
					setDisabled(mButtonReserve);
					setDisabled(mButtonLeaving);
				} else {
					if (mApplication.getState() == SparkingApplication.STATE_IDLE) {
						setEnabled(mButtonSearch);
						setEnabled(mButtonReserve);
						setDisabled(mButtonLeaving);
						mImageClock.setVisibility(TextView.INVISIBLE);
						mMap.setMyLocationEnabled(true);
					} else if (mApplication.getState() == SparkingApplication.STATE_SEARCHING) {
						setPressed(mButtonSearch);
						setEnabled(mButtonReserve);
						setDisabled(mButtonLeaving);
						mImageClock.setVisibility(TextView.INVISIBLE);
						mMap.setMyLocationEnabled(true);
					} else if (mApplication.getState() == SparkingApplication.STATE_PARKED) {
						setDisabled(mButtonSearch);
						setPressed(mButtonReserve);
						setEnabled(mButtonLeaving);
						mImageClock.setVisibility(TextView.INVISIBLE);
						mMap.setMyLocationEnabled(false);
						mMap.addMarker(setLeaver(mApplication.getLeaver()));
					} else if (mApplication.getState() == SparkingApplication.STATE_LEAVING) {
						setDisabled(mButtonSearch);
						setDisabled(mButtonReserve);
						setPressed(mButtonLeaving);
						mMap.setMyLocationEnabled(true);
						mMap.addMarker(setLeaver(mApplication.getLeaver()));
					}
				}
				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
						new LatLng(mApplication.getLatitude(), mApplication.getLongitude()), ZOOM_DEFAULT));
			}
		});
	}

	@Override
	public void deliverResponse(Intent i) {
		if (mLoading != null) {
			mLoading.dismiss();
		}
		if (i.getAction().equals(SparkingService.RESULT_NEW_SEARCHER_SUCCEEDED)) {
			SearcherResponse sr = null;
			try {
				sr = new SearcherResponse(i.getStringExtra(SparkingService.EXTRAS_NEW_SEARCHER));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if (VerifyJSONObjects.verifySearcher(sr)) {
				setSearcherMarkers(sr);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mApplication.setState(SparkingApplication.STATE_SEARCHING);
						startSchedule();
					}
				});
			}
		} else if (i.getAction().equals(SparkingService.RESULT_NEW_SEARCHER_FAILED)) {
			if (mLoading != null) {
				mLoading.dismiss();
			}
			syncStatus = false;
			PopUpMessage popup = new PopUpMessage("Error con la comunicación con el servidor");
			popup.show(getFragmentManager(), "");
		} else if (i.getAction().equals(SparkingService.RESULT_UPDATE_SEARCHER_SUCCEEDED)) {
			try {
				setSearcherMarkers(new SearcherResponse(i.getStringExtra(SparkingService.EXTRAS_UPDATE_SEARCHER)));
			} catch (JSONException e) {
				// Log.e(TAG, "Error call setSearcherMarkers.");
			}
		} else if (i.getAction().equals(SparkingService.RESULT_UPDATE_SEARCHER_FAILED)) {
			if (mLoading != null) {
				mLoading.dismiss();
			}
			syncStatus = false;
			PopUpMessage popup = new PopUpMessage("Error con la comunicación con el servidor");
			popup.show(getFragmentManager(), "");
		} else if (i.getAction().equals(SparkingService.RESULT_DESTROY_SEARCHER_SUCCEEDED)) {
			syncStatus = false;
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					cancelSchedule();
					mApplication.setSearcher(null);
					if (mPressendSearch) {
						mApplication.setState(SparkingApplication.STATE_IDLE);
					} else {
						mApplication.setState(SparkingApplication.STATE_PARKED);
						mApplication.setLeaver(makeLeaver());
					}
				}
			});
		} else if (i.getAction().equals(SparkingService.RESULT_DESTROY_SEARCHER_FAILED)) {
			if (mLoading != null) {
				mLoading.dismiss();
			}
			syncStatus = false;
			PopUpMessage popup = new PopUpMessage("Error con la comunicación con el servidor");
			popup.show(getFragmentManager(), "");
		} else if (i.getAction().equals(SparkingService.RESULT_NEW_LEAVER_SUCCEEDED)) {
			try {
				mApplication.setLeaver(new Leaver(i.getStringExtra(SparkingService.EXTRAS_NEW_LEAVER)));
			} catch (JSONException e) {
				// Log.i(TAG, "Error to parse object Parked received.");
			}
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mImageClock.setText(mApplication.getLeaver().getTimer() - 1 + ":59");
					mImageClock.setVisibility(TextView.VISIBLE);
					mApplication.setState(SparkingApplication.STATE_LEAVING);
					makeClock(mApplication.getLeaver().getTimer());
				}
			});
		} else if (i.getAction().equals(SparkingService.RESULT_NEW_LEAVER_FAILED)) {
			if (mLoading != null) {
				mLoading.dismiss();
			}
			syncStatus = false;
			PopUpMessage popup = new PopUpMessage("Error con la comunicación con el servidor");
			popup.show(getFragmentManager(), "");
		} else if (i.getAction().equals(SparkingService.RESULT_UPDATE_LEAVER_SUCCEEDED)) {
			try {
				mApplication.setLeaver(new Leaver(i.getStringExtra(SparkingService.EXTRAS_UPDATE_LEAVER)));
			} catch (JSONException e) {
				// Log.i(TAG, "Error to parse object Parked received.");
			}
		} else if (i.getAction().equals(SparkingService.RESULT_UPDATE_LEAVER_FAILED)) {
			if (mLoading != null) {
				mLoading.dismiss();
			}
			syncStatus = false;
			PopUpMessage popup = new PopUpMessage("Error con la comunicación con el servidor");
			popup.show(getFragmentManager(), "");
		} else if (i.getAction().equals(SparkingService.RESULT_DESTROY_LEAVER_SUCCEEDED)) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					syncStatus = false;
					mApplication.setState(SparkingApplication.STATE_IDLE);
					mApplication.setLeaver(null);
					int points = ((mTimeSelected - mTimeElapsed) * MAX_POINTS) / mTimeSelected;
					PopUpMessage popup = new PopUpMessage("Has ganado " + points + " puntos \n de 10 posibles");
					popup.show(getFragmentManager(), "");
					mApplication.setPoints(points);
				}
			});
		} else if (i.getAction().equals(SparkingService.RESULT_DESTROY_LEAVER_FAILED)) {
			if (mLoading != null) {
				mLoading.dismiss();
			}
			syncStatus = false;
			PopUpMessage popup = new PopUpMessage("Error con la comunicación con el servidor");
			popup.show(getFragmentManager(), "");
		}
		setLayout();
	}

	private void setSearcherMarkers(SearcherResponse response) {
		mLeaversList = response.getParkeds();
		mSearchersList = response.getSearchers();
		// mServerTime = response.getServerTime();

		Searcher s = response.getSearcher();

		mApplication.setSearcher(s);
		syncStatus = false;

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				paintMarkers();
			}
		});
	}

	private void refreshMarkers() {
		mMap.clear();
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				paintMarkers();
			}
		});
	}

	private void paintMarkers() {
		if (mSearchersList != null) {
			Iterator<Searcher> i = mSearchersList.listIterator();
			while (i.hasNext()) {
				mMap.addMarker(setSearcher(i.next()));
			}
		}
		if (mLeaversList != null) {
			Iterator<Leaver> i = mLeaversList.listIterator();
			while (i.hasNext()) {
				Marker location = mMap.addMarker(setLeaver(i.next()));
				location.showInfoWindow();
			}
		}
	}

	private MarkerOptions setSearcher(Searcher searcher) {
		return new MarkerOptions().position(new LatLng(searcher.getLatitude(), searcher.getLongitude())).icon(
				BitmapDescriptorFactory.fromResource(R.drawable.bullet_red_small));
	}

	private MarkerOptions setLeaver(Leaver leaver) {
		if (leaver.getTimer() == 0) {
			return new MarkerOptions().position(new LatLng(leaver.getLatitude(), leaver.getLongitude())).icon(
					BitmapDescriptorFactory.fromResource(R.drawable.logo_verde_38px));
		} else {
			return new MarkerOptions().position(new LatLng(leaver.getLatitude(), leaver.getLongitude()))
					.icon(BitmapDescriptorFactory.fromResource(R.drawable.logo_verde_38px))
					.title(leaver.getTimer() + "min");
		}
	}

	public void setPressed(Button button) {
		button.setBackground(getResources().getDrawable(R.drawable.button_googlemap_active));
		button.setTextColor(getResources().getColor(android.R.color.primary_text_light));
		button.setClickable(true);
	}

	public void setEnabled(Button button) {
		button.setBackground(getResources().getDrawable(R.drawable.button_googlemap_enabled));
		button.setTextColor(getResources().getColor(android.R.color.secondary_text_light));
		button.setClickable(true);
	}

	public void setDisabled(Button button) {
		button.setBackground(getResources().getDrawable(R.drawable.button_googlemap_unpressed_disabled));
		button.setTextColor(getResources().getColor(android.R.color.tertiary_text_light));
		button.setClickable(false);
	}

	private BroadcastReceiver mSearchersReceiver = new BroadcastReceiver() {
		@SuppressWarnings("unchecked")
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(GcmIntentService.RESULT_UPDATE_LEAVER)) {
				Leaver leaver = null;
				try {
					leaver = new Leaver(intent.getStringExtra(GcmIntentService.EXTRAS_UPDATE_LEAVER));
				} catch (JSONException e) {
					// Log.e(TAG, "Parse parked error.");
				}
				if (leaver.getAction().equals("leave")) {
					boolean rmv = true;
					int i = 0;
					while (rmv) {
						if (mLeaversList.get(i).getId() == leaver.getId()) {
							rmv = false;
						} else {
							i = i + 1;
						}
					}
					mLeaversList.remove(i);
				} else {
					mLeaversList.add(leaver);
				}
				refreshMarkers();
			} else if (intent.getAction().equals(SearcherTaskService.RESULT_UPDATE_SEARCHER)) {
				JSONObject response = null;
				JSONObject searchers = null;
				try {
					response = new JSONObject(intent.getStringExtra(SearcherTaskService.EXTRAS_UPDATE_SEARCHER));
					if (!response.has("searchers")) {
						// Log.d(TAG,
						// "This message doesn't contains searchers");
					} else {
						searchers = response.getJSONObject("searchers");
						mSearchersList.clear();
						Iterator<String> i = searchers.keys();
						while (i.hasNext()) {
							String key = i.next();
							mSearchersList.add(new Searcher(searchers.getString(key)));
						}

					}
				} catch (JSONException e) {
					// Log.e(TAG, "Parse searchers error.");
				}
				refreshMarkers();
			}
		}
	};

	@Override
	public void onTimeSelected(int type, int value) {
		if (mService == null) {
			// Log.i(TAG, "Service error.");
		} else {
			if (type == PopUpSelectTime.TIME) {
				Leaver l = mApplication.getLeaver();
				l.putTimer((value + 1) * 5);
				mLoading = new LoadingProgressDialog();
				mLoading.show(getFragmentManager(), "");
				if (!mService.newLeaver(MainActivity.this, l)) {
					// Log.e(TAG, "Wrong call.");
				}
			} /*
			 * else if (type == PopUpSelectTime.METERS) {
			 * getWindow().addFlags(WindowManager
			 * .LayoutParams.FLAG_KEEP_SCREEN_ON); syncStatus = true; mLoading =
			 * new LoadingProgressDialog(); mLoading.show(getFragmentManager(),
			 * ""); if (!mService.newSearcher(MainActivity.this,
			 * makeSearcher((value + 1) * 500))) { //Log.e(TAG, "Wrong call.");
			 * } }
			 */
		}
	}

	private void makeClock(final int out_time) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mTimeSelected = out_time;
				mCountDownTimer = new CountDownTimer(TimeUnit.MINUTES.toMillis(out_time), 1000) {
					@SuppressLint("DefaultLocale")
					@Override
					public void onTick(long millisUntilFinished) {
						String value = String.format(
								"%02d:%02d\nSalir",
								TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
								TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
										- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
												.toMinutes(millisUntilFinished)));
						Spannable span = new SpannableString(value);
						span.setSpan(new RelativeSizeSpan(0.6f), 6, value.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						mTimeElapsed = (int) TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
						mImageClock.setText(span);
					}

					@Override
					public void onFinish() {
						syncStatus = true;
						Leaver p = mApplication.getLeaver();
						p.putGcmId(mApplication.getGcmId());
						mLoading = new LoadingProgressDialog();
						mLoading.show(getFragmentManager(), "");
						if (!mService.removeLeaver(MainActivity.this, p)) {
							// Log.e(TAG, "Wrong call.");
						}
					}
				}.start();
			}
		});
	}
}
