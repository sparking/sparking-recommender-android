package cat.sparking;

import org.json.JSONException;

import android.app.Application;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import cat.sparking.restclientbbva.models.Leaver;
import cat.sparking.restclientbbva.models.Searcher;

public class SparkingApplication extends Application {
	public static final String TAG = SparkingApplication.class.getCanonicalName();

	public static Typeface typeRobotoBlack;
	public static Typeface typeRobotoBold;
	public static Typeface typeRobotoLight;
	public static Typeface typeRobotoMedium;
	public static Typeface typeRobotoRegular;
	public static Typeface typeRobotoThin;

	public static final String PREF_STATE = "state";
	public static final String PREF_SEARCHER = "searcher";
	public static final String PREF_LEAVER = "leaver";
	public static final String PREF_GCMID = "gcm_id";
	public static final String PREF_POINTS = "points";

	public static final String STATE_IDLE = "idle";
	public static final String STATE_SEARCHING = "searching";
	public static final String STATE_PARKED = "parked";
	public static final String STATE_LEAVING = "leaving";
	
	public static final String LONGITUDE = "longitude";
	public static final String LATITUDE = "latitude";

	private SharedPreferences mPreferences;
	private String mState = null;
	private Searcher mSearcher = null;
	private Leaver mLeaver = null;
	private String mGcmId = null;
	private double mLatitude = 0;
	private double mLongitude = 0;
	private int mPoints = 0;

	@Override
	public void onCreate() {
		mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		typeRobotoBlack = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Black.ttf");
		typeRobotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");
		typeRobotoLight = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
		typeRobotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
		typeRobotoRegular = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		typeRobotoThin = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Thin.ttf");
	}

	public String getState() {
		if (mState == null) {
			return STATE_IDLE;
		} else {
			return mState;
		}
	}

	public void setState(String value) {
		mState = value;
	}
	
	public int getPoints() {
		return mPoints;
	}
	
	public void setPoints(int value) {
		mPoints = mPoints + value;
	}
	
	public double getLongitude() {
		return mLongitude;
	}
	
	public double getLatitude() {
		return mLatitude;
	}
	
	public void setLocation(Double latitude, Double longitude) {
		mLatitude = latitude;
		mLongitude = longitude;
	}

	public Searcher getSearcher() {
		if (mSearcher != null) {
			if (!mSearcher.has("gcm_id")) {
				mSearcher.putGcmId(getGcmId());
			}
		}
		return mSearcher;
	}

	public void setSearcher(Searcher searcher) {
		mSearcher = searcher;
	}

	public Leaver getLeaver() {
		if (mLeaver != null) {
			if (!mLeaver.has("gcm_id")) {
				mLeaver.putGcmId(getGcmId());
			}
		}
		return mLeaver;
	}

	public void setLeaver(Leaver leaver) {
		mLeaver = leaver;
	}

	public String getGcmId() {
		return mGcmId;
	}

	public void setGcmId(String value) {
		mGcmId = value;
	}

	public SharedPreferences getPreferences() {
		return mPreferences;
	}
	
	public void loadPreferences() {
		mState = mPreferences.getString(PREF_STATE, STATE_IDLE);
		mGcmId = mPreferences.getString(PREF_GCMID, null);
		mPoints = mPreferences.getInt(PREF_POINTS, 0);
		try {
			String value = mPreferences.getString(PREF_SEARCHER, null);
			if (value != null) {
				mSearcher = new Searcher(value);
			}
			value = mPreferences.getString(PREF_LEAVER, null);
			if (value != null) {
				mLeaver = new Leaver(value);
			}
		} catch (JSONException e) {
			//Log.i(TAG, "Loadpreferences error.");
		}
	}

	public void savePreferences() {
		SharedPreferences.Editor e = mPreferences.edit();
		if (mState != null) {
			e.putString(PREF_STATE, mState);
		}
		if (mSearcher != null) {
			e.putString(PREF_SEARCHER, mSearcher.toString());
		}
		if (mLeaver != null) {
			e.putString(PREF_LEAVER, mLeaver.toString());
		}
		if (mGcmId != null) {
			e.putString(PREF_GCMID, mGcmId);
		}
		if (mPoints != 0) {
			e.putInt(PREF_POINTS, mPoints);
		}
		e.commit();
	}

}
