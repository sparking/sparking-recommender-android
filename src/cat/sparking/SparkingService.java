package cat.sparking;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import cat.sparking.restclientbbva.LeaverRestClient;
import cat.sparking.restclientbbva.SearcherRestClient;
import cat.sparking.restclientbbva.models.Leaver;
import cat.sparking.restclientbbva.models.Searcher;

public class SparkingService extends Service {

	public final static String TAG = SparkingService.class.getCanonicalName();

	// Intent Actions
	public final static String RESULT_NEW_SEARCHER_FAILED = "result_new_searcher_failed";
	public final static String RESULT_NEW_SEARCHER_SUCCEEDED = "result_new_searcher_succeeded";
	public final static String RESULT_UPDATE_SEARCHER_FAILED = "result_update_searcher_failed";
	public final static String RESULT_UPDATE_SEARCHER_SUCCEEDED = "result_update_searcher_succeeded";
	public final static String RESULT_DESTROY_SEARCHER_FAILED = "result_remove_searcher_failed";
	public final static String RESULT_DESTROY_SEARCHER_SUCCEEDED = "result_remove_searcher_succeeded";

	public final static String RESULT_NEW_LEAVER_FAILED = "result_new_leaver_failed";
	public final static String RESULT_NEW_LEAVER_SUCCEEDED = "result_new_leaver_succeeded";
	public final static String RESULT_UPDATE_LEAVER_FAILED = "result_update_leaver_failed";
	public final static String RESULT_UPDATE_LEAVER_SUCCEEDED = "result_update_leaver_succeeded";
	public final static String RESULT_DESTROY_LEAVER_FAILED = "result_remove_leaver_failed";
	public final static String RESULT_DESTROY_LEAVER_SUCCEEDED = "result_remove_leaver_succeeded";
	
	public final static String RESULT_INTERNET_FAILED = "result_internet_failed";
	
	// Intent extras
	public final static String EXTRAS_NEW_SEARCHER = "extras_new_searcher";
	public final static String EXTRAS_UPDATE_SEARCHER = "extras_update_searcher";
	public final static String EXTRAS_DESTROY_SEARCHER = "extras_destroy_searcher";

	public final static String EXTRAS_NEW_LEAVER = "extras_new_leaver";
	public final static String EXTRAS_UPDATE_LEAVER = "extras_update_leaver";
	public final static String EXTRAS_DESTROY_LEAVER = "estras_destroy_leaver";

	private final IBinder mBinder = new SparkingServiceBinder();

	@Override
	public void onCreate() {
		super.onCreate();
		registerReceiver(mCheckNetworkStatus, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mCheckNetworkStatus);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}
	
	public boolean newSearcher(final SparkingServiceListener listener, final Searcher searcher) {
		if (listener == null) {
			return false;
		}
		new Thread() {
			public void run() {
				SearcherRestClient rc = new SearcherRestClient();
				JSONObject o = null;
				try {
					o = rc.create(searcher);
					if (o.has("gcm_id")) {
						o = rc.update(Integer.toString(searcher.getId()), searcher);
					}
					Log.d(TAG, o.toString());
				} catch (JSONException e) {
					Log.e(TAG, "Registration error: ", e);
					if (listener != null) {
						Intent i = new Intent();
						i.setAction(RESULT_NEW_SEARCHER_FAILED);
						listener.deliverResponse(i);
						return;
					}
				} catch (NullPointerException e2) {
					Log.e(TAG, "Problem with internet connection");
					if (listener != null) {
						Intent i = new Intent();
						i.setAction(RESULT_INTERNET_FAILED);
						listener.deliverResponse(i);
						return;
					}
				}
				Intent i = new Intent();
				i.setAction(RESULT_NEW_SEARCHER_SUCCEEDED);
				i.putExtra(EXTRAS_NEW_SEARCHER, o.toString());
				listener.deliverResponse(i);
			}
		}.start();
		return true;
	}
	
	public boolean updateSearcher(final SparkingServiceListener listener, final Searcher searcher) {
		if (listener == null) {
			return false;
		}
		new Thread() {
			public void run() {
				SearcherRestClient rc = new SearcherRestClient();
				JSONObject o = null;
				try {
					o = rc.update(Integer.toString(searcher.getId()), searcher);
					Log.i("RETURN: ", o.toString());
				} catch (JSONException e) {
					Log.e(TAG, "Registration error: ", e);
					if (listener != null) {
						Intent i = new Intent();
						i.setAction(RESULT_UPDATE_SEARCHER_FAILED);
						listener.deliverResponse(i);
						return;
					}
				} catch (NullPointerException e2) {
					Log.e(TAG, "Problem with internet connection");
					if (listener != null) {
						Intent i = new Intent();
						i.setAction(RESULT_INTERNET_FAILED);
						listener.deliverResponse(i);
						return;
					}
				}
				Intent i = new Intent();
				i.setAction(RESULT_UPDATE_SEARCHER_SUCCEEDED);
				i.putExtra(EXTRAS_UPDATE_SEARCHER, o.toString());
				listener.deliverResponse(i);
			}
		}.start();
		return true;
	}
	
	public boolean removeSearcher(final SparkingServiceListener listener, final Searcher searcher) {
		if (listener == null) {
			return false;
		}
		new Thread() {
			public void run() {
				SearcherRestClient rc = new SearcherRestClient();
				JSONObject o = null;
				try {
					Searcher s = new Searcher();
					s.putGcmId(searcher.getGcmId());
					s.putAction("DELETE");
					Log.i("AA", s.toString());
					o = rc.update(Integer.toString(searcher.getId()), s);
				} catch (JSONException e) {
					Log.e(TAG, "Registration error: ", e);
					if (listener != null) {
						Intent i = new Intent();
						i.setAction(RESULT_UPDATE_SEARCHER_FAILED);
						listener.deliverResponse(i);
						return;
					}
				} 
				if (o == null) {
					Intent i = new Intent();
					i.setAction(RESULT_DESTROY_SEARCHER_SUCCEEDED);
					listener.deliverResponse(i);
				}
			}
		}.start();
		return true;
	}
	
	public boolean newLeaver(final SparkingServiceListener listener, final Leaver leaver) {
		if (listener == null) {
			return false;
		}
		new Thread() {
			public void run() {
				LeaverRestClient rc = new LeaverRestClient();
				Leaver s = null;
				try {
					s = rc.create(leaver);
					Log.i("RETURN: ", s.toString());
				} catch (JSONException e) {
					Log.e(TAG, "Registration error: ", e);
					if (listener != null) {
						Intent i = new Intent();
						i.setAction(RESULT_NEW_LEAVER_FAILED);
						listener.deliverResponse(i);
						return;
					}
				} catch (NullPointerException e2) {
					Log.e(TAG, "Problem with internet connection");
					if (listener != null) {
						Intent i = new Intent();
						i.setAction(RESULT_INTERNET_FAILED);
						listener.deliverResponse(i);
						return;
					}
				}
				Intent i = new Intent();
				i.setAction(RESULT_NEW_LEAVER_SUCCEEDED);
				i.putExtra(EXTRAS_NEW_LEAVER, s.toString());
				listener.deliverResponse(i);
			}
		}.start();
		return true;
	}
	
	public boolean updateLeaver(final SparkingServiceListener listener, final Leaver leaver) {
		if (listener == null) {
			return false;
		}
		new Thread() {
			public void run() {
				LeaverRestClient rc = new LeaverRestClient();
				JSONObject s = null;
				try {
					s = rc.update(Integer.toString(leaver.getId()), leaver);
					Log.i("RETURN: ", s.toString());
				} catch (JSONException e) {
					Log.e(TAG, "Registration error: ", e);
					if (listener != null) {
						Intent i = new Intent();
						i.setAction(RESULT_UPDATE_LEAVER_FAILED);
						listener.deliverResponse(i);
						return;
					}
				} catch (NullPointerException e2) {
					Log.e(TAG, "Problem with internet connection");
					if (listener != null) {
						Intent i = new Intent();
						i.setAction(RESULT_INTERNET_FAILED);
						listener.deliverResponse(i);
						return;
					}
				}
				Intent i = new Intent();
				i.setAction(RESULT_UPDATE_LEAVER_SUCCEEDED);
				i.putExtra(EXTRAS_UPDATE_LEAVER, s.toString());
				listener.deliverResponse(i);
			}
		}.start();
		return true;
	}
	
	public boolean removeLeaver(final SparkingServiceListener listener, final Leaver leaver) {
		if (listener == null) {
			return false;
		}
		new Thread() {
			public void run() {
				LeaverRestClient rc = new LeaverRestClient();
				JSONObject s = null;
				try {
					Leaver p = new Leaver();
					p.putGcmId(leaver.getGcmId());
					p.putAction("DELETE");
					s = rc.update(Integer.toString(leaver.getId()), p);
				} catch (JSONException e) {
					Log.e(TAG, "Registration error: ", e);
					if (listener != null) {
						Intent i = new Intent();
						i.setAction(RESULT_DESTROY_LEAVER_FAILED);
						listener.deliverResponse(i);
						return;
					}
				} 
				if (s == null) {
					Intent i = new Intent();
					i.setAction(RESULT_DESTROY_LEAVER_SUCCEEDED);
					listener.deliverResponse(i);
				}
			}
		}.start();
		return true;
	}
		
	// Makes the public methods of the service available on bind
	public class SparkingServiceBinder extends Binder {
		public SparkingService getService() {
			return SparkingService.this;
		}
	}

	public interface SparkingServiceListener {
		public void deliverResponse(Intent i);
	}

	// This checks if there is internet connection
	private final BroadcastReceiver mCheckNetworkStatus = new BroadcastReceiver() {

		@Override
		public void onReceive(Context c, Intent intent) {
			ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
			boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
			if (isConnected) {
				// Have internet Connection
			} else {
				// Do not have internet connection
			}
		}
	};
}
