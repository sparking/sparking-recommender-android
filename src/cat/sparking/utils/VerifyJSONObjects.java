package cat.sparking.utils;

import cat.sparking.restclientbbva.models.SearcherResponse;

public class VerifyJSONObjects {
	
	public final static String[] mFieldsSearcher = {"leavers", "searchers", "server_time", "searcher"};
	
	public static boolean verifySearcher (SearcherResponse searcher) {
		for (int x=0; x<mFieldsSearcher.length; x++) {
			if (!searcher.has(mFieldsSearcher[x])) {
				return false;
			}
		}
		return true;
	}
}
