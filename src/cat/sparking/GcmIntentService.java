package cat.sparking;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import cat.sparking.restclientbbva.models.Leaver;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmIntentService extends IntentService {
	private static final String TAG = GcmIntentService.class.getCanonicalName();

	public final static String RESULT_UPDATE_LEAVER = "result_update_leaver_succeeded";
	public final static String EXTRAS_UPDATE_LEAVER = "extras_update_leaver";

	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	private LocalBroadcastManager mLBM;

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle

			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				// sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				// sendNotification("Deleted messages on server: " +
				// extras.toString());
			// If its's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				Log.i(TAG, "Received: " + extras.toString());
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		Leaver leaver = new Leaver();
		Log.i(TAG, extras.toString());
		leaver.putAction(extras.getString("action"));
		if (extras.getString("action").equals("leave")) {
			leaver.putId(extras.getString("id"));
		} 
		else {
			leaver.putId(extras.getString("id"));
			leaver.putTimer(Integer.valueOf(extras.getString("timer")));
			leaver.putLatitude(Double.valueOf(extras.getString("lat")));
			leaver.putLongitude(Double.valueOf(extras.getString("lng")));
		}
		sendLeaver(leaver.toString());
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendLeaver(String leaver) {
		Intent intent = new Intent(RESULT_UPDATE_LEAVER);
		intent.putExtra(EXTRAS_UPDATE_LEAVER, leaver);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}
}
