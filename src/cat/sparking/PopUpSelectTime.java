package cat.sparking;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

public class PopUpSelectTime extends DialogFragment {
	public static final int METERS = 0;
	public static final int TIME = 1;
	
	private OnTimeSelectedListener mListener;
	
	private NumberPicker mNumberPicker;
	private Button mButton;
	private TextView mTextTitle;
	private TextView mTextUnits;
	
	private int mType;
	private String mTitle;
	

	@SuppressLint("ValidFragment")
	public PopUpSelectTime(int type, String title) {
		mType = type;
		mTitle = title;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		getDialog().setCancelable(false);
		View v = inflater.inflate(R.layout.popup_estimated_leaving, container);
		
		mNumberPicker = (NumberPicker) v.findViewById(R.id.numberPicker1);
		mButton = (Button) v.findViewById(R.id.button_popup_time);
		mTextTitle = (TextView) v.findViewById(R.id.textPopUpTitle);
		mTextUnits = (TextView) v.findViewById(R.id.textPopUpUnits);
		
		mButton.setTypeface(SparkingApplication.typeRobotoMedium);
		mTextTitle.setTypeface(SparkingApplication.typeRobotoMedium);
		mTextUnits.setTypeface(SparkingApplication.typeRobotoMedium);
		
		mNumberPicker.setMaxValue(3);
		mNumberPicker.setMinValue(0);

		if (mType == TIME) {
			mNumberPicker.setDisplayedValues(new String[] {"5", "10", "15", "20"});
			mTextTitle.setText(mTitle);
			mTextUnits.setText("min");
		} else if (mType == METERS) {
			mNumberPicker.setDisplayedValues(new String[] {"500", "1000", "1500", "2000"});
			mTextTitle.setText(mTitle);
			mTextUnits.setText("metres");
		}

		mButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
					onTimeSet(mNumberPicker.getValue());
					dismiss();
				}
		});

		return v;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			this.mListener = (OnTimeSelectedListener)activity;
		}
		catch (final ClassCastException e) {
			throw new ClassCastException(activity.toString());
		}
	}
	
	public void onTimeSet(int value) {
		this.mListener.onTimeSelected(mType, value);
	}
	
	public interface OnTimeSelectedListener {
		public void onTimeSelected(int type, int value);
	}
}