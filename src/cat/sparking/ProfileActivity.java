package cat.sparking;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class ProfileActivity extends Activity {

	private TextView mTextPointsTitle;
	private TextView mTextPointsValue;

	private SparkingApplication mApplication;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_profile);
		
		mApplication = (SparkingApplication) getApplication();
		
		mTextPointsTitle = (TextView) findViewById(R.id.profile_textview_title_points);
		mTextPointsValue = (TextView) findViewById(R.id.profile_textview_value_points);

		mTextPointsTitle.setTypeface(SparkingApplication.typeRobotoBold);
		mTextPointsValue.setTypeface(SparkingApplication.typeRobotoRegular);
		
		mTextPointsValue.setText(""+ mApplication.getPoints());
	}
}
